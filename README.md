# Object Oriented Java Projects From College: Alex's student repository

This repository holds Java code that was created for my classes from CCAC. (SPRING 2022)
It's been a while since this class, so I don't remember the actual assignment names.

# CIT-244 OBJECT-ORIENTED DESIGN AND DATA STRUCTURES USING JAVA

CIT-244 is the class that follows up CIT-130. I believe it was my third Java clas at CCAC.
All assignments covered here will be from this class. 
Assignemnt 1 and 6  is not with the other files. I'm pretty sure they were not programming assignment.

# Assignment 2: FriendsAndFamily

This assignment comes with three files. A family and friend class. The file that you should run is FriendsAndFamily.java.

# Assignment 3: Caesar Ciphor

Comes with two files. One file to run and the other a txt file with scarmbled passwords.

# Assignment 4: Sort & Search Algorithms

Comes with four files. One txt file with a bunch of random numbers. A file with the search algorithms and a one with sort algorithms.
Lastly, the file to run named SortSearchAlgorithm_Wagner.java.
This assignment tests out multiple search and sort algorithms to find the fastest and most effeiecnt one of each.

# Mini Assignment: Pushing and Popping stacks

Reads Java files and determines if contained { } are balanced or not using stacks and lists.

# Assignment 5: Using jsoup for Web Scraping

Program to prompt then find how many times a team won the Super Bowl. 
However, since it has not been updated in a few years some of the jsoup imports have broken. Thus, the program does not work currently.

# Assignment 7/Final Project: Making an API

Creates a GUI using an API that you can use to make your own hockey roster.
Once program is running you can enter the players name or jersey number.
There are two file. You should run he one named API_LineupMaker_Template.java.
