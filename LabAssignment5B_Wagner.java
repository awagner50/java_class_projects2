// CIT244 - Lab Assignment 5B
// Alex Wagner 04/24/22
//Program to prompt then find how many times a team won the Super Bowl

import java.io.IOException;
import java.util.Scanner;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.jsoup.nodes.Document;

public class LabAssignment5B_Wagner {

    public static void main(String[] args) throws IOException {
        // Variables
        String[] list = new String[350];
        String[] champs =  new String[57];
        Scanner input = new Scanner(System.in);
        String teamName;
        
        // Load in Super Bowl Champions webiste
        Document doc = Jsoup.connect("https://blog.ticketcity.com/nfl/super-bowl/super-bowl-champions/").get();
        
        // Elements object to hold all "td" elements on page
        Elements tdE = doc.select("td");
        
        // Enhanced for loop to take all the td's in the 2nd index and put them into list
        int i = 0;
        for (Element td : tdE) {
                list[i] = td.getElementsByIndexEquals(2).text();
                i++;
        }
        
        // For loop to copy list to champs while only keeping team names
        int k = 0;
        for (int j = 0; j < list.length; j++) {
            if (list[j] != null && list[j].length() != 0) {
                champs[k] = list[j];
                k++;
            }
            j++;
        }
        //System.out.println(Arrays.toString(champs)); //Test purposes
        
        // Prompt User for team name
        System.out.println("Enter a NFL team name and I will tell you how many"
                + " times they have won the Super Bowl.");
        System.out.println("Please use full names, use correct grammar and "
                + "capitalization (ex: Pittsburgh Steelers).");
        System.out.print("--> ");
        teamName = input.nextLine();
        System.out.println("");
        
        // Calling name verifier after user input
        nameChecker(teamName);
        
        /*Check how many times the team inputed has won by counting how many
            times the team name is displayed in champs array*/
        int l = 0, count = 0;
        for (String c : champs) {
            if (champs[l].equals(teamName)) {
                count++;
            }
            l++;
        }
        
        //Answer users question
        if (count == 0) {
            System.out.println("I'm sorry to break this to you, but The " + teamName
                + " have never won a Super Bowl D: Better luck next time.");
        } else if (count == 1) {
            System.out.println("Not too shabby, The " + teamName + " have won " 
                    + "the Suber Bowl " + count + " time.");
        } else {
            System.out.println("Oh cool, The " + teamName + " have won the " + 
                    "Super Bowl " + count + " times... Lucky!");
        }
        
    }
    
    // Method to verify user input
    public static void nameChecker(String inputName) throws IOException {
        /* List of NFL teams
           - I tried pulling these from some webisotes but it was giving me trouble
             so I decided to just use the array for now.*/
        String[] nflTeams = {"Arizona Cardinals", "Atlanta Falcons", "Baltimore Ravens", 
            "Buffalo Bills", "Carolina Panthers", "Chicago Bears", "Cincinnati Bengals",
            "Cleveland Browns", "Dallas Cowboys", "Denver Broncos", "Detroit Lions",
            "Green Bay Packers", "Houston Texans", "Indianapolis Colts", "Jacksonville Jaguars",
            "Kansas City Chiefs", "Las Vegas Raiders", "Los Angeles Chargers",
            "Los Angeles Rams", "Miami Dolphins", "Minnesota Vikings", "New England Patriots",
            "New Orleans Saints", "New England Patriots", "New York Giants", "New York Jets",
            "Philadelphia Eagles", "Pittsburgh Steelers", "San Francisco 49ers",
            "Seattle Seahawks", "Tampa Bay Buccaneers", "Tennessee Titans", "Washington Redskins"};
        
        // Checking to see if name inputed is in list
        int i = 0, count = 0;
        for (String t : nflTeams) {
            if (nflTeams[i].equals(inputName)) {
                count++;
            }
            i++;
        }
        
        // If team is not found leave program
        if (count == 0) {
            System.out.println("Team name not found!");
            System.out.println("Please check if grammar and/or capitalization" + 
                         " are correct then try again.");
            System.exit(0);
        }
        
    }
    
}